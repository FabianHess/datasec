Access sql on docker:
# docker exec -it <container id> bash
# mysql -uroot -p

Test database:
show databases;
use datasec;
SHOW tables;
SHOW columns FROM users;
SELECT * FROM users;

service mysql start
mysql_secure_installation
- Set password
