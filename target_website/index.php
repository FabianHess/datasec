<?php
    session_start();
    $subscription = null;
    $cookie_name = "subscription";

    if (isset($_COOKIE[$cookie_name])) {
        $subscription = $_COOKIE[$cookie_name];
    }

    $advertisment_block='<a href="https://www.heise.de"><img alt="heise" src="advert/heise.png"></a>';
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Zielwebseite</title>
        <link rel="icon" href="images/icon_rwu.ico">
        <link href="styles/style.css" rel="stylesheet" type="text/css">
    </head>

    <body>
        <div class="header">
            <img src="images/logo_rwu.png" alt="logo">
            <h1>Zielwebseite</h1>
        </div>

        <div class="navbar">
            <?php
                $subscription = null;

                if (isset($_COOKIE['subscription'])){
                  $subscription = $_COOKIE['subscription'];
                }


#                $ip_addrs = array();
#                if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
#                    $ip_addrs[] = $_SERVER['HTTP_CLIENT_IP'];
#                } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
#                    $ip_addrs[] = $_SERVER['HTTP_X_FORWARDED_FOR'];
#                } else {
#                    $ip_addrs[] = $_SERVER['REMOTE_ADDR'];
#                }
#                echo "Folgende IP-Adressen konnten gefunden werden:<br>";
#                foreach($ip_addrs as $key => $value) {
#                  echo "- $value<br>";
#                }

                if ($subscription == null) {
                    echo "Sie haben keine Abonnements.<br>";
                } else {
                    echo "Sie haben folgende Abonnement:<br>";
                    echo $subscription;
                }
            ?>

        <div class="main">
            <div class="page_link">
                <?php
                    if ($subscription != null) {
                        include("./articles/sub_article.php");
                    } else {
                        include("./articles/free_article.php");
                    }
                    echo("".$advertisment_block);
                ?>
            </div>
        </div>
    </body>
</html>
