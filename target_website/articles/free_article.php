<h1>Freier Artikel</h1>
<p><b>Dieser Text ist frei verfügbar und kann von nicht eingeloggten Usern eingesehen werden.</b></p>
<p> 
    In diesem Artikel können frei verfügbare Inhalte angezeigt werden. 
    Diese sind ohne Abonnement lesbar und können auch von Nutzern die nicht eingeloggt sind gelesen werden.
</p>