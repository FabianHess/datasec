USE datasec;
CREATE TABLE users (id INT(32) UNSIGNED AUTO_INCREMENT PRIMARY KEY, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, bankaccount VARCHAR(255));
CREATE TABLE subscriptions (id INT(32) AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255) NOT NULL, owner VARCHAR(255) NOT NULL, price DOUBLE(4, 2));
CREATE TABLE user_subscriptions (id INT(32) AUTO_INCREMENT PRIMARY KEY, user_id INT(32) NOT NULL, subscription_id INT(32) NOT NULL);

INSERT INTO users (username, password, bankaccount) VALUES ('user_sub@test.de', '1234', 'DE41 1234 1472 1596 3585');
INSERT INTO users (username, password) VALUES ('user_free@test.de', '1234');
INSERT INTO subscriptions (name, owner, price) VALUES ('Basisabonnement', 'Zielwebseite', 2.5);
INSERT INTO user_subscriptions (user_id, subscription_id) VALUES ((SELECT id FROM users WHERE username='user_sub@test.de'), (SELECT id FROM subscriptions WHERE name='Basisabonnement' AND owner='Zielwebseite' LIMIT 1));
