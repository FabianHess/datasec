<div class="header">
    <img src="images/logo_rwu.png" alt="logo">
    <h1>Datenschutzfreundliche Webseite</h1>
</div>

<div class="navbar">
  <a href="/" <?php if($webpage == "startpage"){?>class="active"<?php } ?>>Home</a>
  <a href="policy.php" <?php if($webpage == "policy"){?>class="active"<?php } ?>>Datenschutzerklärung</a>
  <?php if (!isset($_SESSION['is_logged']) || $_SESSION['is_logged'] == "false") { ?>
  <a href="account.php" <?php if($webpage == "account"){?>class="active"<?php } ?>>Accountverwaltung</a>
  <?php }
    if (!isset($_SESSION['is_logged']) || $_SESSION['is_logged'] == "false") {
  ?>
    <label for="modal-toggle">Anmeldung</label>
  <?php
    } else {
  ?>
    <form action="" method="post">
    <input type="submit" name="exit" value="Abmeldung">
  <?php
    }
  ?>
</div>

<?php
  if (isset($_POST['exit'])) {
    unset($_SESSION['user']);
    $_SESSION['is_logged'] = false;
  }
?>
