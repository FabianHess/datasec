<?php
  if( isset($_COOKIE['setCookieNote'])) {
    $showPopup = false;
  } else {
    $showPopup = true;
  }

  if ($showPopup) {
?>
  <div id="cookie-popup">
    <div class="consentNote">
      <p>Auf dieser Webseite und den hier verlinkten Webseiten werden Cookies verwendet.</p>
      <p>Diese Cookies werden verwendet, um angemeldete Nutzer auf den Zielwebseiten zu registrieren.</p>
      <p>Sollten Sie alle Cookies ablehnen, können die Zielwebseiten unter Umständen nicht wie gewünscht funktionieren.</p>
      <p>Personenbezogene Daten werden hierbei nicht gespeichert.</p>
    </div>
    <br>
    <span class="addInfo">
      <a href="policy.php">Datenschutzerklärung</a>
    </span>
    <form action="" method="post">
      <button onclick="submit">Akzeptieren</button>
      <button onclick="return">Ablehnen</button>
  </div>
<?php
  }

  if(!empty($_POST)) {
    return true;
  } else {
    return false;
  }
?>
