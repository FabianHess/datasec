<?php
  session_start();
?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8" />
        <title>Datenschutzfreundliche Webseite</title>
        <link rel="icon" href="images/icon_rwu.ico">
        <link href="styles/style_account.css" rel="stylesheet" type="text/css">
    </head>

    <body>
      <?php
        $user_pw = null;
        $bank_info = null;
        $subscriptions = null;

        $webpage = "account";
        require("head_body.php");
        require("login.php");

        function get_user_data($user) {
          $u_id = null;
          $u_name = null;
          $user_pw = null;
          $bank_info = null;
          $subscription_id = null;
          $subscriptions = array();
          $owner = null;

          require_once("php_inc/database.php");
          $conn = get_database_connection();

          if ($conn != null) {
            $result = $conn->query("SELECT * FROM users WHERE username=\"$user\"");

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) {
                $u_id = $row['id'];
                $u_name = $row['username'];
                $user_pw = $row['password'];
                $bank_info = $row['bankaccount'];
              }
            }
            if ($u_id != null) {
              $subscription_id_res = $conn->query("SELECT subscription_id FROM user_subscriptions WHERE user_id = \"$u_id\"");
              if ($subscription_id_res ->num_rows > 0) {
                while($row = $subscription_id_res->fetch_assoc()) {
                  $subscription_id = $row['subscription_id'];
                }
              }
              if ($subscription_id != null) {
                $subscriptions_res = $conn->query("SELECT name, owner FROM subscriptions WHERE id = \"$subscription_id\"");
                if ($subscriptions_res->num_rows > 0) {
                  while($row = $subscriptions_res->fetch_assoc()) {
                    $subscriptions[] = $row['name'];
                    $owner = $row['owner'];
                  }
                }
              }
            }
          }

          return [$user_pw, $bank_info, $subscriptions, $owner];
        }

        function set_user_data($user_name, $bank_info) {
          $success = false;
          require_once("php_inc/database.php");
          $conn = get_database_connection();
          $query = "UPDATE users SET bankaccount = \"$bank_info\" WHERE username = \"$user_name\"";

          if ($conn != null) {
            $success = $conn->query($query);
          }

          return $success;
        }

        function valid_bank_info($bank_info) {
          if (preg_match("/[A-Z]{2}[0-9]{2}(?:[ ]?[0-9]{4}){4}(?!(?:[ ]?[0-9]){3})(?:[ ]?[0-9]{1,2})?/", $bank_info)) {
            return true;
          }
          echo ("Keine validen Bankinformationen");
          return false;
        }

        # Is user logged in
        if(isset($_SESSION['user'])) {
            $user = $_SESSION['user'];
            [$user_pw, $bank_info, $subscriptions, $owner] = get_user_data($user);

            if (isset($_POST['save'])) {
              if (valid_bank_info($_POST['bank']) && $_POST['bank'] != $bank_info) {
                set_user_data($user, $_POST['bank']);
              }
            } elseif (isset($_POST['delete'])) {
              echo "Deleted";
            }
        ?>
        <div class="main">
          <div class="container">
              <form action="" method="post">
                  <div class="row">
                      <div class="col-25">
                          <label for="uname">Nutzername</label>
                      </div>
                      <div class="col-75">
                          <input type="text" id="uname" name="uname" value="<?php echo $user; ?>" readonly size="50">
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-25">
                          <label for="pw">Passwort</label>
                      </div>
                      <div class="col-75">
                         <button type="button" id="pw_change" name="pw_change">Passwort ändern</button>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-25">
                          <label for="bank">IBAN:</label>
                      </div>
                    <div class="col-75">
                      <input type="text" id="bank" name="bank" value="<?php echo $bank_info; ?>" size="50">
                    </div>
                  </div>
                    <div class="row">
                      <div class="col-25">
                          <label for="country">Abonnements</label>
                      </div>
                      <div class="col-75">
                          <select id="subscription" name="subscription">
                            <?php
                              if (!empty($subscriptions)) {
                                foreach ($subscriptions as $sub) {
                            ?>
                            <option value=<?php echo $sub ?>><?php echo $sub ?></option>
                            <?php
                                }
                              } else {
                            ?>
                            <option value=""></option>
                            <?php
                              }
                            ?>
                          </select>
                          <?php
                            if (!empty($subscriptions)) {
                              echo "$subscriptions[0] von $owner"
                          ?>
                           <ul>
                            <li>
                              <button>Abmelden</button>
                            </li>
                          </ul>
                          <?php
                            }
                           ?>
                      </div>
                      </div>
                      <div class="row">
                          <input type="reset" value="Abbrechen">
                          <input type="submit" name="save" value="Speichern">
                          <input type="submit" name="delete" value="Account löschen">
                      </div>
            </form>
          </div>
        </div>
        <?php
          }
        ?>
    </body>
</html>
