<?php
  # Session start and cookie permission
  session_start();
  if (!isset($_SESSION['is_logged'])) {
    $_SESSION['is_logged'] = "false";
  }
  require_once("php_inc/database.php");
  $conn = get_database_connection();
  $cookie_set = null;
  ob_start();
  if(require ('cookie_popup.php')) {
    setCookie("setCookieNote", "true", time()+3600*24*30);
    $cookie_set = true;
  } else {
    $cookie_set = false;
  }
  ob_flush();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Datenschutzfreundliche Webseite</title>
        <link rel="icon" href="images/icon_rwu.ico">
        <link href="styles/style.css" rel="stylesheet" type="text/css">
    </head>

    <body>
      <?php
        $target_ip = "20.100.0.100";
        $proxy_url = "";
        $target_url = "http://" . $proxy_url . $target_ip;
        $webpage = "startpage";
        # Upper toolbar import
        require("head_body.php");
        # Login window import
        require("login.php");
      ?>

      <div class="main">
          <h2>Besuchbare Webseiten</h2>
          <div class="page_link">
              <img src="images/info_icon.webp" alt="info">
              <a href=<?php echo $target_url ?>>Zielwebseite</a>
          </div>
          <div class="page_link">
              <a href="https://www.whatismyip.com/de/">IP-Adresse</a>
          </div>
      </div>
    </body>
</html>
