<?php

function get_database_connection() {
  $host = 'my_sql';
  $dbname = 'datasec';
  $username = 'user';
  $password = 'password';
  $conn = null;

  $conn = new mysqli($host, $username, $password, $dbname);
  if ($conn->connect_error) {
      echo "Connection failed: " . $conn->connect_error;
  }

  return $conn;
}

function close_connection($conn) {
  $conn->close();
}


// function db_startup($startup) {
//     if($startup) {
//       $host = 'mysql';
//       $dbname = 'datasec';
//       $username = 'user';
//       $password = 'password';
//
//       $conn = new mysqli($host, $username, $password, $dbname);
//       $startup = false;
//       # echo "Connected to $dbname at $host successfully.";
//       if ($conn->connect_error) {
//           echo("Could not connect to the database" + $dbname + ".<br>");
//       }
//       try {
//           if(isset($_SESSION['userid'])) {
//               //Abfrage der Nutzer ID vom Login
//               $userid = $_SESSION['userid'];
//
//               echo "Hallo User: ".$userid;
//               $sql = $pdo->query("SELECT email, password FROM users");
//
//               foreach($sql as $row) {
//                   echo $row["email"] . "<br>";
//               }
//           }
//       }
//       catch(PDOException $e) {
//           echo "Error: " . $e->getMessage();
//       }
//   }

?>
