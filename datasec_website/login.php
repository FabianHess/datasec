<div class="Scriptcontent">
  <!-- Login Form Popup HTML -->
  <input id="modal-toggle" type="checkbox">
  <label class="modal-backdrop" for="modal-toggle"></label>
  <div class="modal-content">
      <label class="modal-close-btn" for="modal-toggle">
          <svg width="30" height="30">
              <line x1="5" y1="5" x2="20" y2="20"/>
              <line x1="20" y1="5" x2="5" y2="20"/>
        </svg>
      </label>
    <div class="tabs">
      <!--  LOG IN  -->
      <input class="radio" id="tab-1" name="tabs-name" type="radio" checked>
      <label for="tab-1" class="table"><span>Login</span></label>
      <div class="tabs-content">
        <form action="" method="post">
          <input type="email" name="email" placeholder="E-Mail" required>
          <input type="password" name="password" placeholder="Password" required>
          <input type="submit" value="Anmelden">
        </form>
        <form class="forgot-password" action="">
          <input id="forgot-password-toggle" type="checkbox">
          <label for="forgot-password-toggle">forgot password?</label>
          <div class="forgot-password-content">
            <input type="email" placeholder="enter your email" required>
            <input type="submit" value="go">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<?php
  function login($user, $password) {
    $logged_in = false;
    require_once("php_inc/database.php");
    $conn = get_database_connection();
    $u_name = null;
    $u_pw = null;

    if ($conn != null) {
      $result = $conn->query("SELECT * FROM users WHERE username=\"$user\"");

      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          $u_name = $row['username'];
          $u_pw = $row['password'];
        }
      }
      if ($u_name == $user && $u_pw == $password) {
        $_SESSION['user'] = $u_name;
        $logged_in = true;
      }
    }

    # password_verify($password, password_hash($user['password'], PASSWORD_DEFAULT)

    return $logged_in;
  }

  function set_target_cookie() {
    $proxy_url = "";
    $target_url = "http://20.100.0.100";
    echo "NOT HERE!!!";
    $cookie_set_url = $proxy_url .$target_url ."/set_cookie.php";
    $ch = curl_init($cookie_set_url);
    curl_setopt($ch, CURLOPT_URL, $cookie_set_url);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_exec($ch);
    curl_close($ch);
  }

  if (!isset($_SESSION['user'])) {
    if (isset($_POST['email']) && isset($_POST['password'])) {
      $email = $_POST['email'];
      $password = $_POST['password'];

      $logged_in = login($email, $password);

      if ($logged_in) {
        set_target_cookie();
        $_SESSION['is_logged'] = "true";
      }
    }
  }
?>
