<div class="Scriptcontent">
    <?php
        $abo = "Basisabonnement";
        $target = "Zielwebseite";
        $text = "Hiermit akzeptieren Sie die Nutzungsbedingungen für das Basisabonnement der Zielwebseite.
In diesem Textfeld muss das angebotene Abonnement, die Zielwebseite und der Inhalt des Abonnements dargestellt werden.
Zusätzlich ist es notwendig, dass der Preis und die Kündigungsfrist enthalten sind.

Alle Informationen die hier angegeben werden sind in der Verantwortung der Betreiber der Zielwebseiten.";

    ?>
    <!-- Login Form Popup HTML -->
    <input id="modal-toggle" type="checkbox">
    <label class="modal-backdrop" for="modal-toggle"></label>
    <div class="modal-content">
        <label class="modal-close-btn" for="modal-toggle">
            <svg width="30" height="30">
                <line x1="5" y1="5" x2="20" y2="20"/>
                <line x1="20" y1="5" x2="5" y2="20"/>
          </svg>
        </label>
        <div class="tabs">
            <!--  LOG IN  -->
            <input class="radio" id="tab-1" name="tabs-name" type="radio" checked>
            <label for="tab-1" class="table"><span>Anmeldung <?php echo "$abo" ?></span></label>
           <div class="tabs-content">
                <form action="?login=1" method="post">
                    <textarea id="subject" name="subject" placeholder="<?php echo "$text" ?>" style="height:200px" readonly></textarea>
                    <input type="submit" value="Akzeptieren">
                </form>
        </div>
    </div>
</div>
