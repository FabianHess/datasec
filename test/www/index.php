<!DOCTYPE html>	

<html>
    <head>
        <meta charset="UTF-8" />
        <title>Datenschutzauslagerung</title>
        <link rel="icon" href="images/icon_rwu.ico">
        <link href="styles/style.css" rel="stylesheet" type="text/css">
    </head>
    
    <body>
        <div class="header">
            <img src="images/logo_rwu.png" alt="logo">
            <h1>Datenschutzauslagerung</h1>
        </div>
    
        <div class="navbar">
          <a href="#" class="active">Home</a>
          <a href="#">Link</a>
          <a href="#">Link</a>
          <label for="modal-toggle">Login/Signup</label>  
        </div>

        <div class="main">
            <h2>Besuchbare Webseiten</h2>
            <div class="page_link">
                <img src="images/info_icon.webp" alt="info">
                <a href="webpage.php">User-Informationen</a>
            </div>
            <div class="page_link">
                <img src="images/heise_logo.png" alt="info">
                <a href="https://heise.de">heise online</a>
            </div>
        </div>



        <div class="Scriptcontent">
            <!-- Login Form Popup HTML -->
            <input id="modal-toggle" type="checkbox">
            <label class="modal-backdrop" for="modal-toggle"></label>
            <div class="modal-content">
                <label class="modal-close-btn" for="modal-toggle">
                    <svg width="30" height="30">
                        <line x1="5" y1="5" x2="20" y2="20"/>
                        <line x1="20" y1="5" x2="5" y2="20"/>
                  </svg>
                </label>
                <div class="tabs">
                    <!--  LOG IN  -->
                    <input class="radio" id="tab-1" name="tabs-name" type="radio" checked>
                    <label for="tab-1" class="table"><span>Login</span></label>
                    <div class="tabs-content">
                        <form action="?login=1" method="post">
                            <input type="email" name="email" placeholder="E-Mail" required>
                            <input type="password" name="password" placeholder="Password" required>
                            <input type="submit" value="Log In">
                        </form>
                        <form class="forgot-password" action="">
                            <input id="forgot-password-toggle" type="checkbox">
                            <label for="forgot-password-toggle">forgot password?</label>
                            <div class="forgot-password-content">
                                <input type="email" placeholder="enter your email" required>
                                <input type="submit" value="go">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        

    <?php 
        session_start();
        $startup = true;
        
        if($startup) {
            $host = 'localhost';
            $dbname = 'test';
            $username = 'root';
            $password = '';
             
            try {
                $pdo = new PDO("mysql:host=$host;dbname=$dbname;port=3308", $username, $password);
                $startup = false;
                # echo "Connected to $dbname at $host successfully.";
            } catch (PDOException $pe) {
                die("Could not connect to the database $dbname :" . $pe->getMessage()) . "<br>";
            }
            try {
                if(isset($_SESSION['userid'])) {
                    //Abfrage der Nutzer ID vom Login
                    $userid = $_SESSION['userid'];

                    echo "Hallo User: ".$userid;
                    $sql = $pdo->query("SELECT email, password FROM users");
                
                    foreach($sql as $row) {
                        echo $row["email"] . "<br>";
                    }
                }
            }
            catch(PDOException $e) {
                echo "Error: " . $e->getMessage();
            }    
        }
               
        if($_SERVER['REQUEST_METHOD'] == 'POST' and isset($_GET['login'])) {
            echo "Login.";
            login();
        }
        
        if(isset($errorMessage)) {
            echo $email . "<br>" ;
            echo $password . "<br>";
            echo $user['email'] . $user['password'] . "<br>";
            echo $errorMessage;
        }
        
        function login() {
            //TODO:
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "test";

            // Create connection
            $conn = new mysqli($servername, $username, $password);

            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }
            printf("Connected successfully<br>");

            try {
              $conn = new PDO("mysql:host=$servername;dbname=test", $username, $password);
              // set the PDO error mode to exception
              $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
              echo "Connected successfully<br>";
            } catch(PDOException $e) {
              echo "Connection failed: " . $e->getMessage();
            }


            $pdo = new PDO('mysql:host=localhost;dbname=test', 'root', '');
            $email = null;
            $password = null;

            if(isset($_GET['login'])) {
                $email = $_POST['email'];
                $password = $_POST['password'];
                
                $statement = $pdo->prepare("SELECT * FROM users WHERE email = '" . $email . "'");
                $result = $statement->execute(array('email' => $email));
                $user = $statement->fetch();

                //Überprüfung des Passworts
                if ($user !== false && password_verify($password, password_hash($user['password'], PASSWORD_DEFAULT))) {
                    $_SESSION['userid'] = $user['id'];
                    die('Login erfolgreich. Weiter zu <a href="index.php">internen Bereich</a>');
                } else {
                    $errorMessage = "E-Mail oder Passwort war ungültig<br>";
                    echo "Fehler";
                }
                
            }
            $pdo = null; 
        }
    ?>

    </body>
</html>