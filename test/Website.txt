# Aufbau
Es sollen zwei Testwebseiten erstellt werden.
Eine beinhaltet alle personenbezogene Datenverarbeitung und soll als Login-Bereich fungieren.
Die andere Testwebseite soll als Validierungswebseite die Funktionen des Aufbaus aufzeigen.

# Testaufbau der Webseite
Der Webserver, der die beiden Testwebseiten beinhalten, wird auf einen Raspberry Pi aufgesetzt.

Hierbei soll php und sql verwendet werden.

Die Datenbank enthält alle personenbezogenen Daten, die benötigt werden um Nutzern einen Login zu ermöglichen und die notwendigen Privilegien zu gewähren. 
Dazu ist es nötig einen Nutzernamen/E-Mail-Adresse und Passwort-Hash und den Zugriff auf die Webseiten zu sichern.
Der Zugriff ist notwendig, wenn Webseiten unterschiedliche "Plus"-Angebote haben.
Zusätzlich werden noch Bezahlinformationen gesichert, um Zahlungen für "Plus"-Angebote abwickeln zu können.

Bsp: Süddeutsche Zeitung bietet drei Plus-Angebote: Basis, Wochenende und Komplett. Dadurch müssten vier Level abgedeckt sein, damit diese Seite besucht werden kann. 








# Einrichtung der Testumgebung
Es wurde ein Raspberry Pi verwendet.
Um MySQL/MariaDB zu verwenden wurde mariadb-server installiert
'''$ sudo apt-get install mariadb-server'''
'''sudo mysql_secure_installation '''

Root-Passwort: datenschutz


https://r00t4bl3.com/post/how-to-install-mysql-mariadb-server-on-raspberry-pi
